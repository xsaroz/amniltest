<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Company extends Model
{
    protected $fillable = ['name', 'email', 'logo', 'website'];

    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id');
    }

    public function getCompanyEmployees()
    {
        $assignedEmployees = Employee::where('company_id', $this->id)->get();
        $item = [];
        foreach ($assignedEmployees as $key => $employee) {
            $item[$key] = [
                'id' => uniqid(),
                'content' => $employee->full_name,
                'emp_id' => $employee->id
            ];
        }

        return $assignedEmployees->count() ? $item : [];
    }
}
