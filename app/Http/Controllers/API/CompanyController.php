<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Employee;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function store(Request $request)
    {
        if($request->hasFile('logo')) {
            $original_name=$request->file('logo')->getClientOriginalName(); 
            $extension=$request->file('logo')->getClientOriginalExtension();
    
            $name = $original_name.'.'.$extension;
            $path = $request->file('logo')->storeAs('public/uploads',$name);
            $request->logo = $name;
        }
        Company::create($request->all());
        return response()->json(['success' => true], 200);
    }

    public function get_data()
    {
        $companies = Company::all();
        $data = [];
        $notAssignedEmployees = Employee::where('company_id', null)->get();
        $items = [];
        foreach ($notAssignedEmployees as $key => $employee) {
            $items[$key] = [
                'id' => uniqid(),
                'content' => $employee->full_name,
                'emp_id' => $employee->id
            ];
        }

        $data[uniqid()] = [
            'name' => 'Employees',
            'items' => $items
        ];

        foreach($companies as $key => $company) {
            $data[uniqid()] = [
                'name' => $company->name,
                'items' => $company->getCompanyEmployees()
            ];
        }   
        return response()->json(['success' => 'true', 'data' => $data]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $column = $data['column']['name'];
        $columnItem = $data['items'];
        $source = $data['source']['name'];
        $sourceItem = $data['source']['items'];

        $company = Company::where('name', $column)->first();
        foreach ($columnItem as $item) {
            $employee = Employee::find($item['emp_id']);
            $employee->update(['company_id'=> $company ? $company->id : null]);
        }
        return response()->json(['success' => true, 'data' => [$company]], 200);
    }
}
