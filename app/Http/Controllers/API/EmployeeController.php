<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function store(Request $request)
    {
        Employee::create($request->all());

        return response()->json(['success' => true], 200);
    }
}
