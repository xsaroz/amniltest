<?php

namespace App;

use App\Company;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'company_id',
        'email',
        'phone'
    ];

    protected $appends = ['full_name'];

    public function company()
    {
        $this->belongsTo(Company::class, 'company_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name." ".$this->last_name;
    }
}