import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

class Header extends Component {
    // 1.1
    constructor(props) {
        super(props);
        this.state = {
            user: props.userData,
            isLoggedIn: props.userIsLoggedIn
        };
        this.logOut = this.logOut.bind(this);
    }
    // 1.2
    logOut() {
        let appState = {
            isLoggedIn: false,
            user: {}
        };
        localStorage["appState"] = JSON.stringify(appState);
        this.setState(appState);
        this.props.history.push("/login");
    }
    // 1.3
    render() {
        const aStyle = {
            cursor: "pointer"
        };

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">
                    Navbar
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        {/* <li className="nav-item">
                            <a className="nav-link" href="#">
                                <Link to="/">Index</Link>
                            </a>
                        </li> */}
                        {this.state.isLoggedIn ? (
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                    <Link to="/dashboard">Dashboard</Link>
                                </a>
                            </li>
                        ) : (
                            ""
                        )}
                        {this.state.isLoggedIn ? (
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                    <Link to="/employee">Employee</Link>
                                </a>
                            </li>
                        ) : (
                            ""
                        )}
                        {this.state.isLoggedIn ? (
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                    <Link to="/company">Company</Link>
                                </a>
                            </li>
                        ) : (
                            ""
                        )}
                        {!this.state.isLoggedIn ? (
                            <li className="nav-item">
                                <a href="#" className="nav-link"></a>
                                <Link to="/login">Login</Link>
                            </li>
                        ) : (
                            ""
                        )}
                    </ul>
                </div>
            </nav>
        );
    }
}
export default withRouter(Header);
