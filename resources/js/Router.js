import React from "react";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import Login from "./views/Login";
import NotFound from "./views/NotFound";
// User is LoggedIn
import PrivateRoute from "./PrivateRouter";
import Dashboard from "./views/Dashboard";
import Employee from "./views/Employee";
import Company from "./views/Company";

const Main = props => (
    <Switch>
        {/*User might LogIn*/}
        <Route exact path="/" component={Home} />
        {/*User will LogIn*/}
        <Route path="/login" component={Login} />
        {/* User is LoggedIn*/}
        <PrivateRoute path="/dashboard" component={Dashboard} />
        <PrivateRoute path="/employee" component={Employee} />
        <PrivateRoute path="/company" component={Company} />
        {/*Page Not Found*/}
        <Route component={NotFound} />
    </Switch>
);
export default Main;
