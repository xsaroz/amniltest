import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
// import Main from "./Router";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Home from "./components/Home";
import Login from "./views/Login";
import NotFound from "./views/NotFound";
// User is LoggedIn
import PrivateRoute from "./PrivateRouter";
import Dashboard from "./views/Dashboard";
import Employee from "./views/Employee";
import Company from "./views/Company";

class Index extends Component {
    constructor() {
        super();
        this.state = {
            dashboardData: {}
        };
        this.getDashboardData = this.getDashboardData.bind(this);
    }

    componentWillMount() {
        this.getDashboardData();
    }

    async getDashboardData() {
        let res = await axios.get("/api/company/get-data");
        let response = res.data;
        this.setState({ dashboardData: response.data });
    }

    render() {
        return (
            <BrowserRouter>
                {/* <Route component={Main} /> */}
                <Switch>
                    {/*User might LogIn*/}
                    <Route exact path="/" component={Home} />
                    {/*User will LogIn*/}
                    <Route path="/login" component={Login} />
                    <Route
                        path="/dashboard"
                        render={props => (
                            <Dashboard
                                dashboardData={this.state.dashboardData}
                                {...props}
                            />
                        )}
                    />
                    <PrivateRoute path="/employee" component={Employee} />
                    <PrivateRoute path="/company" component={Company} />
                    {/*Page Not Found*/}
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
}
ReactDOM.render(<Index />, document.getElementById("index"));
