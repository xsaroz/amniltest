import React, { Component } from "react";
import Header from "../components/Header";
import Axios from "axios";
import SimpleReactValidator from "simple-react-validator";

class Company extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            user: {},
            name: "",
            email: "",
            logo: "",
            website: ""
        };
        this.validator = new SimpleReactValidator();

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleLogoChange = this.handleLogoChange.bind(this);
        this.handleWebsiteChange = this.handleWebsiteChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // check if user is authenticated and storing authentication data as states if true
    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({
                isLoggedIn: AppState.isLoggedIn,
                user: AppState.user
            });
        }
    }
    handleNameChange(event) {
        this.setState({ name: event.target.value });
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handleLogoChange(event) {
        this.setState({ logo: event.target.value });
    }
    handleWebsiteChange(event) {
        this.setState({ website: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        let data = {
            name: this.state.name,
            email: this.state.email,
            website: this.state.website,
            logo: this.state.logo
        };
        let options = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data"
        };
        axios.post("/api/company/store", data, options).then(res => {
            const response = res.data;
            if (response.success) {
                alert("Data Saved Successfully.");
                this.setState({
                    name: "",
                    email: " ",
                    website: " ",
                    logo: null
                });
            }
        });
    }
    render() {
        return (
            <div>
                <Header
                    userData={this.state.user}
                    userIsLoggedIn={this.state.isLoggedIn}
                />
                <div className="row">
                    <div className="container-fluid">
                        <div className="card card-body mt-3 col-12">
                            <form
                                onSubmit={this.handleSubmit}
                                encType="multipart/form-data"
                                method="post"
                            >
                                <div className="form-group">
                                    <label>Name:</label>
                                    <input
                                        type="text"
                                        value={this.state.name}
                                        onChange={this.handleNameChange}
                                        className="form-control"
                                        name="name"
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Email:</label>
                                    <input
                                        type="text"
                                        value={this.state.email}
                                        onChange={this.handleEmailChange}
                                        className="form-control"
                                        name="email"
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Logo:</label>
                                    <input
                                        type="file"
                                        value={this.state.logo}
                                        onChange={this.handleLogoChange}
                                        className="form-control"
                                        name="logo"
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Website:</label>
                                    <input
                                        type="text"
                                        value={this.state.website}
                                        onChange={this.handleWebsiteChange}
                                        className="form-control"
                                        name="website"
                                    />
                                </div>
                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                        <div className="col-6"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Company;
