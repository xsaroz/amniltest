import React, { Component } from "react";
import Header from "../components/Header";

class Employee extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            user: {},
            first_name: "",
            last_name: "",
            email: "",
            phone: ""
        };
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // check if user is authenticated and storing authentication data as states if true
    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({
                isLoggedIn: AppState.isLoggedIn,
                user: AppState.user
            });
        }
    }

    handleFirstNameChange(event) {
        this.setState({ first_name: event.target.value });
    }

    handleLastNameChange(event) {
        this.setState({ last_name: event.target.value });
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handlePhoneChange(event) {
        this.setState({ phone: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        let data = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            phone: this.state.phone
        };
        axios.post("/api/employee", data).then(res => {
            const response = res.data;
            if (response.success) {
                alert("Data Saved Successfully.");
                this.setState({
                    first_name: " ",
                    last_name: " ",
                    email: " ",
                    phone: " "
                });
            }
        });
    }
    // 4.1
    render() {
        return (
            <div>
                <Header
                    userData={this.state.user}
                    userIsLoggedIn={this.state.isLoggedIn}
                />
                <div className="row">
                    <div className="container-fluid">
                        <div className="card card-body mt-3 col-12">
                            <form
                                onSubmit={this.handleSubmit}
                                encType="multipart/form-data"
                                method="post"
                            >
                                <div className="form-group">
                                    <label>First Name:</label>
                                    <input
                                        type="text"
                                        value={this.state.first_name}
                                        onChange={this.handleFirstNameChange}
                                        className="form-control"
                                        name="first_name"
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Last Name:</label>
                                    <input
                                        type="text"
                                        value={this.state.last_name}
                                        onChange={this.handleLastNameChange}
                                        className="form-control"
                                        name="last_name"
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label>email:</label>
                                    <input
                                        type="email"
                                        value={this.state.email}
                                        onChange={this.handleEmailChange}
                                        className="form-control"
                                        name="email"
                                    />
                                </div>
                                <div className="form-group">
                                    <label>phone:</label>
                                    <input
                                        type="text"
                                        value={this.state.phone}
                                        onChange={this.handlePhoneChange}
                                        className="form-control"
                                        name="phone"
                                    />
                                </div>
                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                        <div className="col-6"></div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Employee;
