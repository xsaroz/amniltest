import React, { Component } from "react";
import Header from "../components/Header";
import uuid from "uuid/v4";
import App from "../components/DragDrop";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            user: {},
            dashboardData: this.props.dashboardData
        };
        console.log(this.props.dashboardData);
    }

    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({
                isLoggedIn: AppState.isLoggedIn,
                user: AppState.user
            });
        }
    }

    render() {
        return (
            <div>
                <Header
                    userData={this.state.user}
                    userIsLoggedIn={this.state.isLoggedIn}
                />
                <App dashboardData={this.state.dashboardData} />
            </div>
        );
    }
}
export default Dashboard;
